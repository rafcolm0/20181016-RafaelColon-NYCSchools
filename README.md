# NYCSchoolsLister
* Developer: Rafael J. Colon
* Email: rafael.colon5@gmail.com
* Phone: 614-218-0648
* Ressume attached to repo.

## NOTES:
 * Tech used: Swift, Alamofire, SwiftyJSON, Toast_Swift
 * UI TEST: Provided only one all-around comprehensive test cases that basically goes through the entire app UI flow: starts up the app, waits (maximum of 20 seconds) for all data to download and then tap on each school entry from the main screen to check for any crashes.  In a real case scenario, I would have checked table view entries against real expected values and behaviors. 
 * Unit test: Wasn't sure how to approach singular test cases as most of the application is based on UI flows.  However, some possible real case testing scenarios would be: check for JSON data validity, check JSON parsers to handle when nodes are not available, create objects that would carry the JSON data and validate them against the JSON nodes, etc.
